import { useState } from "react";

function MovieDetails(props) {
  const { title, description, poster } = props;

  // charger (si c'est possible) la valeur du localStorage
  const currentFavouriteValue =
    localStorage.getItem("newStateFavourite") === "true" ? true : false;

  // comment il se base pour savoir si c'est vrai ou faux quand c'est pas "true" ou "false"
  // truthy / falsy : 0, false, "", null, undefined : "plutôt faux" : falsy

  const [favourite, setFavourite] = useState(currentFavouriteValue);

  function toggleFavourites() {
    // ici je vais pouvoir enregistrer si mon film est en favoris ou pas !
    const newFavouriteValue = !favourite;
    setFavourite(newFavouriteValue); // si c'est faux, ça devient vrai, et inversement

    // enregistrer le nouvel état "favourite" dans le localStorage
    localStorage.setItem("newStateFavourite", newFavouriteValue);
    /*
    if (favourite === true) {
      setFavourite(false);
    } else {
      setFavourite(true);
    }
    */
  }

  return (
    <div>
      <h3>{title}</h3>
      <p>{description}</p>
      <img
        className="movie-poster"
        src={poster}
        alt={title}
        style={{ maxWidth: "80%" }}
      />
      <button onClick={toggleFavourites}>
        {favourite ? "Retirer des favoris" : "Ajouter aux favoris"}
      </button>
    </div>
  );
}

export default MovieDetails;
