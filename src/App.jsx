import { useState } from "react";
import "./App.css";
import MovieDetails from "./components/MovieDetails";

function App() {
  const movie = {
    title: "Alien",
    description: "Ça fait peur",
    poster:
      "https://m.media-amazon.com/images/I/6160ipHE64L._AC_UF1000,1000_QL80_.jpg",
  };

  return (
    <>
      <MovieDetails {...movie} />
    </>
  );
}

export default App;
